# EMS

## backend start
<br />
cd ems-backend
<br />
npm install
<br />
node .
<br />
http://localhost:3000/explorer/

## frontend start
<br />
cd ems-frontend
<br />
npm install
<br />
ng serve
<br />
http://localhost:4200

## test
<br />
cd ems-frontend
<br />
ng test

