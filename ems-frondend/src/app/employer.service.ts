
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployerService {

  uri = 'http://localhost:3000/api/employes';

  constructor(private http: HttpClient) { }

  addEmployer(name, surname, rate) {
    const obj = {
      name: name,
      surname: surname,
      rate: rate
    };
    console.log(obj);
    this.http.post('http://localhost:3000/api/employes', obj)
        .subscribe(res => console.log('Done'));
  }

  getEmployer() {
    return this
          .http
          .get(`${this.uri}`);
  }
  updateEmployer(name, surname, rate, id) {

    const obj = {
        name: name,
        surname: surname,
        rate: rate
      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
    }
  editEmployer(id) {
    return this
          .http
          .get(`${this.uri}/${id}`);
      }
  deleteEmployer(id) {
    return this
          .http
          .delete(`${this.uri}/${id}`);
  }
}

