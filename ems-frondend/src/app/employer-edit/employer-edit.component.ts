
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { EmployerService } from '../employer.service';

@Component({
  selector: 'app-employer-edit',
  templateUrl: './employer-edit.component.html',
  styleUrls: ['./employer-edit.component.css']
})
export class EmployerEditComponent implements OnInit {

  employer: any = {};
  employerForm: FormGroup;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private bs: EmployerService,
    private fb: FormBuilder) {
      this.createForm();
 }

  createForm() {
    this.employerForm = this.fb.group({
        name: ['', Validators.required ],
        surname: ['', Validators.required ],
        rate: ['', Validators.required ]
      });
    }

  
  ngOnInit() {
    this.route.params.subscribe(params => {
        this.bs.editEmployer(params['id']).subscribe(res => {
          this.employer = res;
      });
    });
  }
    updateEmployer(name, surname, rate) {
      this.route.params.subscribe(params => {
        this.bs.updateEmployer(name, surname, rate, params['id']);
        this.router.navigate(['employer']);
  });
  }
}
    