import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { EmployerService } from '../employer.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-employer-add',
  templateUrl: './employer-add.component.html',
  styleUrls: ['./employer-add.component.css']
})
export class EmployerAddComponent implements OnInit {

  employerForm: FormGroup;
  constructor(private fb: FormBuilder, 
    private bs: EmployerService,
    private router: Router,) {
    this.createForm();
    
  }

  createForm() {
    this.employerForm = this.fb.group({
      name: ['', Validators.required ],
      surname: ['', Validators.required ],
      rate: ['', Validators.required ]
    });
  }

  addEmployer(name, surname, rate) {
    this.bs.addEmployer(name, surname, rate);
    this.router.navigate(['employer']);

  }

  ngOnInit() {
    
  }

}
