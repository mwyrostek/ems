import { Component, OnInit } from '@angular/core';
import { Employer } from '../employer';
import { EmployerService } from '../employer.service';

@Component({
  selector: 'app-employer-get',
  templateUrl: './employer-get.component.html',
  styleUrls: ['./employer-get.component.css']
})
export class EmployerGetComponent implements OnInit {

  employers: Employer[];

  constructor(private es: EmployerService) {}

  ngOnInit() {
    this.getEmployers();
  }
  deleteEmployer(id) {
    this.es.deleteEmployer(id).subscribe(res => {
      this.getEmployers();
    });
  }
  getEmployers(){
      this.es.getEmployer().subscribe((data: Employer[]) => {
        this.employers = data;
    });
  }
}
