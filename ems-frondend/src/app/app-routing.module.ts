import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployerAddComponent } from './employer-add/employer-add.component';
import { EmployerEditComponent } from './employer-edit/employer-edit.component';
import { EmployerGetComponent } from './employer-get/employer-get.component';

const routes: Routes = [
  {
    path: 'employer/create',
    component: EmployerAddComponent
  },
  {
    path: 'employer/edit/:id',
    component: EmployerEditComponent
  },
  {
    path: 'employer',
    component: EmployerGetComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
