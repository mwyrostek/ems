import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployerAddComponent } from './employer-add/employer-add.component';
import { EmployerGetComponent } from './employer-get/employer-get.component';
import { EmployerEditComponent } from './employer-edit/employer-edit.component';
import { HttpClientModule } from '@angular/common/http';
import { EmployerService } from './employer.service';
// app.module.ts

import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    EmployerAddComponent,
    EmployerGetComponent,
    EmployerEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,


  ],
  providers: [EmployerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
