import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { EmployerGetComponent } from '../../../employer-get/employer-get.component';
import {HttpClientModule} from '@angular/common/http';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('EmployerGetComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule
      ],
      declarations: [
        EmployerGetComponent
      ],
      providers: [
        SlimLoadingBarService
      ],
      schemas: [NO_ERRORS_SCHEMA]

    }).compileComponents();
  }));

  it('should create the employerGetComponent', () => {
    const fixture = TestBed.createComponent(EmployerGetComponent);
    const employerGetComponent = fixture.debugElement.componentInstance;
    expect(employerGetComponent).toBeTruthy();
  });

  it('should render table as thead:Imię, nazwisko, stawka', () => {
    const fixture = TestBed.createComponent(EmployerGetComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('table thead tr td:first-child').textContent).toContain('Imię');
    expect(compiled.querySelector('table thead tr td:nth-child(2)').textContent).toContain('Nazwisko');
    expect(compiled.querySelector('table thead tr td:nth-child(3)').textContent).toContain('Stawka');
  });
});
