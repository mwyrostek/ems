import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../../app.component';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        SlimLoadingBarService
      ],
      schemas: [NO_ERRORS_SCHEMA]

    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'EMS - Employers managment system'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('EMS - Employers managment system');
  });

  it('should render navigation as: Dodanie pracownika, lista pracowników', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.nav-item:first-child a.nav-link').textContent).toContain('Dodanie pracownika');
    expect(compiled.querySelector('.nav-item:last-child a.nav-link').textContent).toContain('lista pracowników');
  });
});
