const express = require('express');
const app = express();
const employerRoutes = express.Router();

// Require Employer model in our routes module
let employer = require('../models/Employer');

// Defined store route
employerRoutes.route('/add').post(function (req, res) {
  let employer = new Employer(req.body);
  employer.save()
    .then(employer => {
      res.status(200).json({'employer': 'employer in added successfully'});
    })
    .catch(err => {
    res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
employerRoutes.route('/').get(function (req, res) {
    Employer.find(function (err, employers){
    if(err){
      console.log(err);
    }
    else {
      res.json(employers);
    }
  });
});

// Defined edit route
employerRoutes.route('employer/edit/:id').get(function (req, res) {
  let id = req.params.id;
  Employer.findById(id, function (err, employer){
      res.json(employer);
  });
});

//  Defined update route
employerRoutes.route('employer/:id').put(function (req, res) {
    Employer.findById(req.params.id, function(err, employer) {
    if (!employer)
      return next(new Error('Could not load Document'));
    else {
        employer.name = req.body.name;
        employer.surname = req.body.surname;
        employer.rate = req.body.rate;

        employer.save().then(employer => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});

// Defined delete | remove | destroy route
// employerRoutes.route('/delete/:id').delete(function (req, res) {
//     Employer.findByIdAndRemove({_id: req.params.id}, function(err, employer){
//         if(err) res.json(err);
//         else res.json('Successfully removed');
//     });
// });

module.exports = employerRoutes;